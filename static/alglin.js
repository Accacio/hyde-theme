
var Font = "Helvetica Neue";

// "IBM Plex Sans",system-ui,-apple-system,BlinkMacSystemFont,"Segoe UI","Roboto","Droid Sans","Ubuntu","Helvetica Neue",Helvetica,Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol"
vec2 = function(X,Y) { return{x:X,y:Y}; };
vec3 = function(X,Y,Z) { return{x:X,y:Y,z:Z}; };
vec4 = function(X,Y,Z,W) { return{x:X,y:Y,z:Z,w:W}; };

function addVec2(vec1,vec2){
    return {x:vec1.x+vec2.x,y:vec1.y+vec2.y}
}
function subVec2(vec1,vec2){
    return {x:vec1.x-vec2.x,y:vec1.y-vec2.y}
}
function scalarMultVec2(a,vec){
    return {x:a*vec.x,y:a*vec.y}
}

function distVec2(vec2_1,vec2_2){
    return Math.sqrt(Math.pow((vec2_1.x-vec2_2.x),2)+Math.pow((vec2_1.y-vec2_2.y),2))
}


function DrawPoint(vec,name,color) {

    context.beginPath()
    context.fillStyle = color;
    context.arc(vec.x, vec.y, 5, 0 , 2 * Math.PI);
    context.fill();

    var normPoints = Math.sqrt(3)/2;
    var normal = vec2(1,1*normPoints);
    normal.magnitude = Math.sqrt(normal.x*normal.x+normal.y*normal.y)
    normal.x=normal.x/normal.magnitude
    normal.y=normal.y/normal.magnitude
    normal.magnitude = Math.sqrt(normal.x*normal.x+normal.y*normal.y)

    context.font = '15pt ' + Font;
    context.fillText(name,vec.x+15*normal.x, vec.y+15*normal.x);
}
function writeText(text,pos){
    var normPoints = Math.sqrt(3)/2;
    var normal = vec2(1,1*normPoints);
    normal.magnitude = Math.sqrt(normal.x*normal.x+normal.y*normal.y)
    normal.x=normal.x/normal.magnitude
    normal.y=normal.y/normal.magnitude

    context.font = '15pt ' + Font;
    context.fillStyle = "black";
    context.fillText(text,pos.x+15*normal.x, pos.y+15*normal.x);

}

function DrawLineSegment(v1,v2,color) {

    context.beginPath()
    context.moveTo(v1.x, v1.y);
    context.lineTo(v2.x, v2.y);
    context.lineWidth = 3;
    context.fillStyle = color;
    context.strokeStyle = color;
    context.stroke();

    var tanPoints = (v2.y-v1.y)/(v2.x-v1.x);
    var normPoints = -1.0/tanPoints;

    // if (normPoints>1) {
    //     normPoints=1;
    // }
    // if (normPoints<-1) {
    //     normPoints=-1;
    // }
    var normal = vec2(1,1*normPoints);
    normal.magnitude = Math.sqrt(normal.x*normal.x+normal.y*normal.y)
    normal.x=normal.x/normal.magnitude;
    normal.y=normal.y/normal.magnitude;
    normal.magnitude = Math.sqrt(normal.x*normal.x+normal.y*normal.y)


}

function DrawHalfLine(v1,v2,color) {
    DrawLineSegment(v1, addVec2(v1,scalarMultVec2(20000,subVec2(v2,v1))),color);
}

function DrawLine(v1,v2,color) {
    DrawLineSegment(addVec2(v1,scalarMultVec2(20000,subVec2(v2,v1))),
                    addVec2(v2,scalarMultVec2(20000,subVec2(v1,v2))),"black");
}
function DrawPerpendicularLine(v1,v2,v3){

    var toA = subVec2(v1,v2)
    var toB = vec2((0)*toA.x+(-1)*toA.y,(1)*toA.x+(0)*toA.y)

    newA=addVec2(v3,toB);
    newB=addVec2(v3,scalarMultVec2(-1,toB))
    DrawLine(newA, newB,"black");
}
